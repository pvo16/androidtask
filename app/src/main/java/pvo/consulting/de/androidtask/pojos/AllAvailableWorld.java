
package pvo.consulting.de.androidtask.pojos;

import com.google.gson.annotations.Expose;

public class AllAvailableWorld {

    @Expose
    private String id;
    @Expose
    private String language;
    @Expose
    private String url;
    @Expose
    private String country;
    @Expose
    private WorldStatus worldStatus;
    @Expose
    private String mapURL;
    @Expose
    private String name;


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }
    public void setLanguage(String language) {
        this.language = language;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


    public WorldStatus getWorldStatus() {
        return worldStatus;
    }

    public void setWorldStatus(WorldStatus worldStatus) {
        this.worldStatus = worldStatus;
    }


    public String getMapURL() {
        return mapURL;
    }
    public void setMapURL(String mapURL) {
        this.mapURL = mapURL;
    }


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

}
