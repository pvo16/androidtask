
package pvo.consulting.de.androidtask.pojos;

import com.google.gson.annotations.Expose;

public class WorldStatus {

    @Expose
    private Integer id;
    @Expose
    private String description;


    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }


    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

}
