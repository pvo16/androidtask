
package pvo.consulting.de.androidtask.pojos;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class Worlds {

    @Expose
    private String serverVersion;
    @Expose
    private List<AllAvailableWorld> allAvailableWorlds = new ArrayList<AllAvailableWorld>();


    public String getServerVersion() {
        return serverVersion;
    }
    public void setServerVersion(String serverVersion) {
        this.serverVersion = serverVersion;
    }


    public List<AllAvailableWorld> getAllAvailableWorlds() {
        return allAvailableWorlds;
    }
    public void setAllAvailableWorlds(List<AllAvailableWorld> allAvailableWorlds) {
        this.allAvailableWorlds = allAvailableWorlds;
    }

}
