package pvo.consulting.de.androidtask.pojos;

/**
 * Created by pvo on 29.05.15.
 */
public class WorldItem {

    public String id, name;


    public WorldItem(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
