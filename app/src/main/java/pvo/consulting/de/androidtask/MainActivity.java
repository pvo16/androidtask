package pvo.consulting.de.androidtask;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pvo.consulting.de.androidtask.pojos.AllAvailableWorld;
import pvo.consulting.de.androidtask.pojos.WorldItem;
import pvo.consulting.de.androidtask.pojos.Worlds;


public class MainActivity extends AppCompatActivity implements
        ItemFragment.OnFragmentInteractionListener {

    private Map<String, String> params = new HashMap<>();
    private Worlds worlds;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // add the login fragment manually, so it can be replaced later
        LoginFragment loginFrag = new LoginFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction trans = fm.beginTransaction();
        trans.add(R.id.fragment_container, loginFrag);
        trans.addToBackStack(null);
        trans.commit();
    }

    /**
     * Method for handling a login click event
     *
     * @param view
     */
    public void performLogin(View view) {
        EditText email = (EditText) findViewById(R.id.email);
        EditText password = (EditText) findViewById(R.id.password);

        params.put(getString(R.string.param_login), email.getText().toString());
        params.put(getString(R.string.param_password), password.getText().toString());

        loadWorlds();
    }

    /**
     * Callback method for list item click event
     *
     * @param id
     */
    @Override
    public void onFragmentInteraction(String id) {

    }

    /**
     * Public method for getting world data
     * Convert worlds into list items
     *
     * @return
     */
    public List<WorldItem> getWorlds() {
        List<WorldItem> items = new ArrayList<>();

        for (AllAvailableWorld world : worlds.getAllAvailableWorlds()) {
            WorldItem item = new WorldItem(world.getId(), world.getName());
            items.add(item);
        }

        return items;
    }


    /**
     * Make a POST request for login and getting data
     * Convert response into POJO
     * Load fragment for displaying worlds
     */
    private void loadWorlds() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = getString(R.string.host_url);

        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Gson gson = new Gson();
                        worlds = gson.fromJson(response, Worlds.class);
                        displayWorlds();
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("TAG", error.toString());
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                params.put(getString(R.string.param_device_type), getDeviceType());
                params.put(getString(R.string.param_device_id), getMacAddress());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("Accept", "application/json");

                return params;
            }
        };

        queue.add(sr);
    }

    /**
     * Load fragment for displaying worlds
     */
    private void displayWorlds() {
        FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
        trans.replace(R.id.fragment_container, new ItemFragment());
        trans.addToBackStack(null);

        trans.commit();
    }

    /**
     * Get current mac address
     *
     * @return
     */
    private String getMacAddress() {
        WifiManager manager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        String macAddress = manager.getConnectionInfo().getMacAddress();

        if (macAddress == null)
            macAddress = getString(R.string.message_no_mac_address);

        return macAddress;
    }

    /**
     * Get device type
     *
     * @return
     */
    private String getDeviceType() {
        String id = String.format("%s %s", android.os.Build.MODEL, android.os.Build.VERSION.RELEASE);

        return id;
    }
}
